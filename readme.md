## Front-End Home page

![Home page](https://www.dropbox.com/s/egoycmmsx9d6jia/laptop_home.png?dl=1)

## Front-End Register page

![Register](https://www.dropbox.com/s/4nm9m713o2dty1a/laptop_register.png?dl=1)

## Front-End Filter page

![Filter](https://www.dropbox.com/s/vayqk5keyberixc/laptop_form.png?dl=1)

## Front-End Item View page

![Item View](https://www.dropbox.com/s/6v6glpujp8bo6ck/laptop_view.png?dl=1)

## Front-End Responsive

![Mobile](https://www.dropbox.com/s/bti480og1w8kv43/mobile.png?dl=1)

