<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Hookers\Models\CommentEloquent\Comment;

class Comments extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = Comment::count();
        $string = 'Comments';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon' => 'voyager-bubble',
            'title' => "{$count} {$string}",
            'text' => "You have {$count} comments in your database. Click on button below to view all comments.",
            'button' => [
                'text' => 'View all comments',
                'link' => route('voyager.comments.index'),
            ],
            'image' => 'img/widget-backgrounds/comments-02.jpg',
        ]));
    }
}
