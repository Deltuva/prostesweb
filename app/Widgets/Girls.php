<?php

namespace App\Widgets;

use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use Arrilot\Widgets\AbstractWidget;
use App\Hookers\Models\GirlEloquent\GirlPublic;

class Girls extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = GirlPublic::count();
        $string = 'Girls';

        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-fire',
            'title'  => "{$count} {$string}",
            'text'   => "You have {$count} girls in your database. Click on button below to view all girls.",
            'button' => [
                'text' => 'View all girls',
                'link' => route('voyager.girl-public.index'),
            ],
            'image' => 'img/widget-backgrounds/girls-01.jpg',
        ]));
    }
}
