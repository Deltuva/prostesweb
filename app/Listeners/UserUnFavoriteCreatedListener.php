<?php

namespace App\Listeners;

use App\Events\UserUnFavoriteWasCreated;
use App\Hookers\Models\FavoriteEloquent\Favorite;
use Helpers;

class UserUnFavoriteCreatedListener
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserUnFavoriteWasCreated $event)
    {
        $ip = Helpers::getIp();
        $isFavorited = Favorite::whereGirlPublicId($event->favId)
            ->first();

        if ($isFavorited) {
            //\Log::info("UnFavorited id({$event->favId})");

            return $isFavorited->delete();
        }
    }
}
