<?php

namespace App\Listeners;

use App\Events\UserFavoriteWasCreated;
use App\Hookers\Models\FavoriteEloquent\Favorite;
use Carbon\Carbon;
use Helpers;

class UserFavoriteCreatedListener
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserFavoriteWasCreated $event)
    {
        $ip = Helpers::getIp();
        $isFavorited = Favorite::whereGirlPublicId($event->favId)
            ->first();

        if (!$isFavorited) {
            //\Log::info("Favorited id({$event->favId})");

            return Favorite::create([
                'girl_public_id' => $event->favId,
                'ip' => $ip,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
