<?php

namespace App\Hookers\Models\CityEloquent;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['name', 'slug'];

    public function favorite()
    {
    	return $this->belongsTo('App\Hookers\Models\FavoriteEloquent\Favorite');
    }

    public function getCityName()
    {
        return $this->attributes['name'];
    }

    public function getSlug()
    {
        return $this->attributes['slug'];
    }
}
