<?php

namespace App\Hookers\Models\SearchEloquent;

use Illuminate\Database\Eloquent\Model;

class SearchField extends Model
{
    const TYPE_CATEGORY_INFO = 'info';
    const TYPE_CATEGORY_PRICE = 'price';
    const TYPE_CATEGORY_DRIVE = 'drive';

    protected $table = 'search_fields';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getType()
    {
        return $this->attributes['type'];
    }
}
