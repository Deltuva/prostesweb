<?php

namespace App\Hookers\Models\FavoriteEloquent;

use Helpers;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    const OPTIONS_GROUP_WON   = [1, 2, 3, 4];
    const OPTIONS_GROUP_TWOO  = [5, 6, 8, 9];

    protected $table = 'favorites';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['girl_public_id', 'ip', 'created_at', 'updated_at'];

    public function publicGirls()
    {
        return $this->hasOne('App\Hookers\Models\GirlEloquent\GirlPublic', 'id', 'girl_public_id');
    }

    public function servicesOptions()
    {
        return $this->hasMany('App\Hookers\Models\GirlEloquent\GirlPublicOption', 'girl_public_id');
    }

    public function servicesOptionsPacketWon()
    {
        return $this->servicesOptions()
            ->select('service_id', 'status')
            ->whereIn('service_id', self::OPTIONS_GROUP_WON);
    }

    public function servicesOptionsPacketTwoo()
    {
        return $this->servicesOptions()
            ->select('service_id', 'status')
            ->whereIn('service_id', self::OPTIONS_GROUP_TWOO);
    }

    public function city()
    {
        return $this->hasOne('App\Hookers\Models\CityEloquent\City', 'id');
    }

    public function hasFavorite()
    {
        $ip = Helpers::getIp();
        $favorites = $this->whereGirlPublicId($this->getId())
            ->whereIp($ip)->first();

        return $favorites;
    }

    public function isFavorited()
    {
        if (!$this->hasFavorite()) {
            return false;
        }
        return true;
    }

    public function getId()
    {
        return $this->attributes['girl_public_id'];
    }
}
