<?php

namespace App\Hookers\Models\UserEloquent;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    const USER_GENDER_MALE = 'male';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar', 'gender', 'is_agreed_n18',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getName()
    {
        return $this->attributes['name'];
    }

    public function isAdministrator()
    {
        return $this->isAdmin();
    }

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function getWelcome()
    {
        return auth()
            ->user()->gender === self::USER_GENDER_MALE ?
        __('Sveikas prisijungęs prie mūsų!') :
        __('Sveika prisijungusi prie mūsų!');
    }

    public function getEmail()
    {
        return $this->attributes['email'];
    }

    public function getGender()
    {
        return $this->attributes['gender'];
    }

    public function getBalance()
    {
        return $this->attributes['balance'];
    }

    public function getAvatar()
    {
        if (isset($this->attributes['avatar'])) {
            return asset('storage/'.$this->attributes['avatar']);
        } else {
            return asset('img/' . $this->attributes['gender'] == self::USER_GENDER_MALE ? 'user-male' : 'user-female' . '.png');
        }
    }
}
