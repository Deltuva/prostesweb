<?php

namespace App\Hookers\Models\CommentEloquent;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $fillable = ['girl_public_id', 'user_id', 'comment', 'ip'];

    public function girl_public() {
        return $this->belongsTo('App\Hookers\Models\GirlEloquent\GirlPublic');
    }

    public function user() {
        return $this->belongsTo('App\Hookers\Models\UserEloquent\User');
    }
}
