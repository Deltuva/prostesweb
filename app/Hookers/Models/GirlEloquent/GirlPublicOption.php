<?php

namespace App\Hookers\Models\GirlEloquent;

use Illuminate\Database\Eloquent\Model;

class GirlPublicOption extends Model
{
    protected $table = 'girl_public_options';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $fillable = ['girl_public_id', 'category_id', 'service_id', 'status'];

    public function public() {
        return $this->belongsTo('App\Hookers\Models\GirlEloquent\GirlPublic');
    }

    public function category()
    {
        return $this->hasOne('App\Hookers\Models\GirlEloquent\GirlServiceCategory', 'id', 'category_id');
    }

    public function service()
    {
        return $this->hasOne('App\Hookers\Models\GirlEloquent\GirlService', 'id', 'service_id');
    }

    public function servicePacketWon()
    {
        return $this->service()->select('*')->whereIn('service_id', [1])->get();
    }

    public function getCategoryName()
    {
        return $this->category->name;
    }

    public function scopeOnlyCheckedOption($query, $checked = 'checked')
    {
        return $query->where('status', $checked);
    }
}
