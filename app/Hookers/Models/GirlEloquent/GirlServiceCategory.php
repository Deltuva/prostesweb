<?php

namespace App\Hookers\Models\GirlEloquent;

use Illuminate\Database\Eloquent\Model;

class GirlServiceCategory extends Model
{
    protected $table = 'girl_service_categories';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = ['name', 'slug'];

    public function services()
    {
        return $this->hasMany('App\Hookers\Models\GirlEloquent\GirlPublicOption', 'category_id');
    }

    public function getCategoryNameAttribute()
    {
        if (isset($this->name)) {
            return "{$this->name}";
        }
    }
}
