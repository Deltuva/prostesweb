<?php

namespace App\Hookers\Models\GirlEloquent;

use Illuminate\Database\Eloquent\Model;

class GirlService extends Model
{
    protected $table = 'girl_services';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('App\Hookers\Models\GirlEloquent\GirlServiceCategory');
    }

    public function getCategoryName()
    {
        return $this->category->name;
    }
}
