<?php

namespace App\Hookers\Models\GirlEloquent;

use Illuminate\Database\Eloquent\Model;

class GirlPhoto extends Model
{
    protected $table = 'girl_photos';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function girl_public()
    {
        return $this->belongsTo('App\Hookers\Models\GirlEloquent\GirlPublic');
    }

    public function getId()
    {
        return $this->attributes['id'];
    }
}
