<?php

namespace App\Hookers\Models\GirlEloquent;

use App\Hookers\Models\FavoriteEloquent\Favorite;
use Helpers;
use Illuminate\Database\Eloquent\Model;

class GirlPublic extends Model
{
    const OPTIONS_GROUP_WON = [1, 2, 3, 4];
    const OPTIONS_GROUP_TWOO = [5, 6, 8, 9];

    protected $table = 'girl_public';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo('App\Hookers\Models\UserEloquent\User');
    }

    public function city()
    {
        return $this->hasOne('App\Hookers\Models\CityEloquent\City', 'id', 'city_id');
    }

    public function servicesOptions()
    {
        return $this->hasMany('App\Hookers\Models\GirlEloquent\GirlPublicOption', 'girl_public_id');
    }

    public function servicesOptionsGrouped()
    {
        return $this->hasMany('App\Hookers\Models\GirlEloquent\GirlPublicOption', 'girl_public_id')
            ->groupBy('category_id');
    }

    public function servicesOptionsPacketWon()
    {
        return $this->servicesOptions()
            ->select('service_id', 'status')
            ->whereIn('service_id', self::OPTIONS_GROUP_WON);
    }

    public function servicesOptionsPacketTwoo()
    {
        return $this->servicesOptions()
            ->select('service_id', 'status')
            ->whereIn('service_id', self::OPTIONS_GROUP_TWOO);
    }

    public function userFavorites()
    {
        return $this->hasMany('App\Hookers\Models\FavoriteEloquent\Favorite');
    }

    public function hasFavorite()
    {
        $ip = Helpers::getIp();
        $favorites = Favorite::whereGirlPublicId($this->getId())
            ->whereIp($ip)->first();

        return $favorites;
    }

    public function isFavorited()
    {
        if (!$this->hasFavorite()) {
            return false;
        }
        return true;
    }

    public function comments()
    {
        return $this->hasMany('App\Hookers\Models\CommentEloquent\Comment');
    }

    public function girl_photo()
    {
        return $this->hasOne('App\Hookers\Models\GirlEloquent\GirlPhoto', 'girl_public_id');
    }

    public function photos()
    {
        return $this->hasMany('App\Hookers\Models\GirlEloquent\GirlPhoto', 'girl_public_id');
    }

    public function getId()
    {
        return $this->attributes['id'];
    }

    public function getCityNameAttribute()
    {
        if (isset($this->city->name)) {
            return "{$this->city->name}";
        }
    }

    public function getPhotoImageAttribute()
    {
        if ($this->girl_photo['name']) {
            if ($this->girl_photo['name'] == false) {
                return asset('img/no-girl.png');
            } else {
                return url('storage/' . $this->girl_photo['name']);
            }
        } else {
            return asset('img/no-girl.png');
        }
    }

    public function getPhoneNumberAttribute()
    {
        if (isset($this->attributes['phone_number'])) {
            //$formatted = "<span class='phone-code'>+ (".substr($this->attributes['phone_number'],0,3).")</span> <strong>".substr($this->attributes['phone_number'],3,3)."-".substr($this->attributes['phone_number'],6,3)."-".substr($this->attributes['phone_number'],9,2)."</strong>";
            return $this->attributes['phone_number'];
        }
    }

    public function phoneNumberLikeSplit($phone_number)
    {
        $formatted = "<span class='phone-code'>+ (" . substr($phone_number, 0, 3) . ")</span> <strong>" . substr($phone_number, 3, 3) . "-" . substr($phone_number, 6, 3) . "-" . substr($phone_number, 9, 2) . "</strong>";
        return $formatted;
    }

    public function getFemaleAgeAttribute()
    {
        if (isset($this->attributes['age']) > 0) {
            return "{$this->attributes['age']}";
        }
    }

    public function getFemaleHeightSizeAttribute()
    {
        if (isset($this->attributes['height_size']) > 0) {
            return "{$this->attributes['height_size']}";
        }
    }

    public function getFemaleWeightSizeAttribute()
    {
        if (isset($this->attributes['weight_size']) > 0) {
            return "{$this->attributes['weight_size']}";
        }
    }

    public function getFemaleBreastSizeAttribute()
    {
        if (isset($this->attributes['breast_size']) > 0) {
            return "{$this->attributes['breast_size']}";
        }
    }

    public function getFemaleClothingSizeAttribute()
    {
        if (isset($this->attributes['clothing_size']) > 0) {
            return "{$this->attributes['clothing_size']}";
        }
    }

    public function getPriceForWonHourAttribute()
    {
        if (isset($this->attributes['price_1hour']) > 0) {
            return "{$this->attributes['price_1hour']}";
        }
    }

    public function getPriceForTwoHourAttribute()
    {
        if (isset($this->attributes['price_2hour']) > 0) {
            return "{$this->attributes['price_2hour']}";
        }
    }

    public function getPriceForThreeHourAttribute()
    {
        if (isset($this->attributes['price_3hour']) > 0) {
            return "{$this->attributes['price_3hour']}";
        }
    }

    public function getPriceForNightAttribute()
    {
        if (isset($this->attributes['price_night']) > 0) {
            return "{$this->attributes['price_night']}";
        }
    }

    public function getCanAnalInServicePacketAttribute()
    {
        if (isset($this->attributes['can_anal']) > 0) {
            return $this->attributes['can_anal'] == true ? '<i class="fas fa-check pull-left" style="font-size: 8px; margin: 5px 4px 0 0; color: #d41e28;"></i>' : 'X';
        }
    }

    public function getCanToApartmentsAttribute()
    {
        if (isset($this->attributes['can_to_apartment']) > 0) {
            return "{$this->attributes['can_to_apartment']}";
        }
    }

    public function getCanAnalAttribute()
    {
        if (isset($this->attributes['can_anal']) > 0) {
            return "{$this->attributes['can_anal']}";
        }
    }

    public function getCanToOfficeAttribute()
    {
        if (isset($this->attributes['can_to_office']) > 0) {
            return "{$this->attributes['can_to_office']}";
        }
    }

    public function getCanToHouseAttribute()
    {
        if (isset($this->attributes['have_apartments']) > 0) {
            return "{$this->attributes['have_apartments']}";
        }
    }
}
