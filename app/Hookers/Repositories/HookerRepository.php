<?php
namespace App\Hookers\Repositories;

use App\Hookers\Models\GirlEloquent\GirlPublic;
use App\Hookers\Repositories\HookerRepositoryInterface;

/**
 * HookerRepository.
 */
class HookerRepository extends Repository implements HookerRepositoryInterface
{
    public function __construct(GirlPublic $hooker)
    {
        $this->model = $hooker;
    }

    /**
     *  Get All Hookers
     *
     * @return void
     */
    public function getAllGirls($category, $city)
    {
        return $this->model
          ->whereCategoryId($category)
          ->whereCityId($city);
    }

    /**
     * Get Hookers By Category
     *
     * @param $categoryId
     * @return void
     */
    public function getGirlsOptionsWhereInIds($options_ids)
    {
        return $this->model
            ->whereHas('options', function ($query) use ($options_ids) {
                return $query->whereIn('service_id', [$options_ids]);
            })
            ->with('options')
            ->get();
    }
}
