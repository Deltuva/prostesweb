<?php
namespace App\Hookers\Repositories;

/**
 * HookerRepositoryInterface interface
 */
interface HookerRepositoryInterface
{
    /**
     * Get All Hookers
     *
     * @return void
     */
    public function getAllGirls($category, $city);

    /**
     * Get Hookers By Category
     *
     * @param $category
     * @return void
     */
    public function getGirlsOptionsWhereInIds($options_ids);
}
