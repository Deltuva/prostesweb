<?php

namespace App\Hookers\Services;

use App\Hookers\Models\CityEloquent\City;

class WebService
{
    /**
     * City
     *
     * @return boolean
     */
    public function hasCity()
    {
        if (!session()->has('city')) {
            return false;

        } else {
            return true;
        }
    }

    /**
     * City
     *
     * @return boolean
     */
    public function currentCityId()
    {
        if (session()->has('city')) {

            $cityModel = new City();

            $currentCity = $cityModel->whereSlug(session()->get('city'))
                ->firstOrFail();

            return $currentCity->id;
        }
    }
}
