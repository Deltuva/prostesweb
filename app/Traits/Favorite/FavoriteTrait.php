<?php
namespace App\Traits\Favorite;

trait FavoriteTrait
{
    public function scopeFavoriteByIp($query)
    {
        $countIps = Helpers::getIp()->count() == 1 ? true : false;

        return $query->whereIp($countIps);
    }
}

trait FavoriteTrait
{
    /**
     * Checked if has user favorited ad
     *
     * @param UserFavorite $favorites
     * @return boolean
     */
    public function hasFavorited()
    {
        return UserFavorite::where('user_id', $this->isCurrentLoggedUser())
           ->where('ad_id', $this->ad_id)
           ->first();
    }
    /**
     * Checked if user favorited ad
     *
     * @return boolean
     */
    public function isFavorited()
    {
        if (!$this->hasFavorited()) {
            return false;
        }
        return true;
    }
}