<?php

namespace App\Classes;

class Filter
{
    /**
     * Checking if Services Array Not Empty
     *
     * @return boolean
     */
    public function hasServicesFilter()
    {
        $request = request()->all();
        if (isset($request) && count($request) > 0) {
            if (!empty($request['filters']['services'])) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Get Services Ids
     *
     * @return array
     */
    public function getServicesIds()
    {
        $request = request()->all();
        $servicesArray = array();
        foreach ($request['filters']['services'] as $key => $value) {
            $servicesArray[] = $value;
        }
        $services = implode(',', $servicesArray);

        return $services;
    }

}
