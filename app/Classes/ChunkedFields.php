<?php

namespace App\Classes;

use App\Classes\Constants;
use App\Hookers\Models\SearchEloquent\SearchField;

class ChunkedFields
{
    /**
     * Get Chunked Fields
     *
     * @return void
     */
    public static function chunkedSearchFields()
    {
        $searchInfoFields = SearchField::whereCategory(Constants::TYPE_CATEGORY_INFO)
            ->orderBy('id', 'asc')
            ->get();
        $infoCollections = collect($searchInfoFields);
        $searchFieldsInfoData = $infoCollections
            ->chunk(2);
        $searchFieldsInfoData
            ->toArray();

        $searchPriceFields = SearchField::whereCategory(Constants::TYPE_CATEGORY_PRICE)
            ->orderBy('id', 'asc')
            ->get();
        $priceCollections = collect($searchPriceFields);
        $searchFieldsPriceData = $priceCollections
            ->chunk(2);
        $searchFieldsPriceData
            ->toArray();

        $searchDriveFields = SearchField::whereCategory(Constants::TYPE_CATEGORY_DRIVE)
            ->orderBy('id', 'asc')
            ->get();
        $driveCollections = collect($searchDriveFields);
        $searchFieldsDriveData = $driveCollections
            ->chunk(2);
        $searchFieldsDriveData
            ->toArray();

        return [
            'searchFieldsInfoData' => $searchFieldsInfoData,
            'searchFieldsPriceData' => $searchFieldsPriceData,
            'searchFieldsDriveData' => $searchFieldsDriveData,
        ];
    }
}
