<?php

namespace App\Classes;

class Constants
{
    // Search Fields Defines
    const TYPE_CATEGORY_INFO = 'info';
    const TYPE_CATEGORY_PRICE = 'price';
    const TYPE_CATEGORY_DRIVE = 'drive';

    // Services Defines
    const TYPE_SERVICE_CHECKED = 'checked';
    const TYPE_SERVICE_UNCHECKED = 'unchecked';

    // Girl Public Defines
    const TYPE_GENDER_MALE = 'm';
    const TYPE_GENDER_FEMALE = 'f';
    const TYPE_CAN_ANAL_FALSE = 0;
    const TYPE_CAN_ANAL_TRUE = 1;
    const TYPE_CAN_TO_APARTMENT_FALSE = 0;
    const TYPE_CAN_TO_APARTMENT_TRUE = 1;
    const TYPE_CAN_TO_SAUNA_FALSE = 0;
    const TYPE_CAN_TO_SAUNA_TRUE = 1;
    const TYPE_CAN_TO_HOTEL_FALSE = 0;
    const TYPE_CAN_TO_HOTEL_TRUE = 1;
    const TYPE_CAN_TO_OFFICE_FALSE = 0;
    const TYPE_CAN_TO_OFFICE_TRUE = 1;
    const TYPE_CAN_HAVE_APARTMENTS_FALSE = 0;
    const TYPE_CAN_HAVE_APARTMENTS_TRUE = 1;

    // Cities
    const TYPE_CITY_VILNIUS = 'vilnius';
}
