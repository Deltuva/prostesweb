<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class UserFavoriteWasCreated
{
    use SerializesModels;

    /**
     * @var favId
     */
    public $favId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($favId)
    {
        $this->favId = $favId;
    }
}
