<?php

namespace App\Http\Middleware;

use Session;
use Closure;

class City
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('city')) {
          $city = Session::get('city');
        }
        return $next($request);
    }
}
