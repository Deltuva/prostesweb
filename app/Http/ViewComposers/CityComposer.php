<?php
namespace App\Http\ViewComposers;

use App\Hookers\Models\CityEloquent\City;
use Cache;
use Illuminate\View\View;

class CityComposer
{
    /**
     * @var $cities
     */
    protected $cities;
    /**
     * Constructor
     */
    public function __construct(City $city)
    {
        $this->cities = $city;
    }
    /**
     * Get Cities
     *
     * @param  View  $view
     */
    public function compose(View $view)
    {
        $cities = Cache::rememberForever('cities', function () {
            return $this->cities::all();
        });
        $view->with('cities', $cities);
    }
}
