<?php
namespace App\Http\ViewComposers;

use App\Hookers\Models\GirlEloquent\GirlService;
use Cache;
use Illuminate\View\View;

class GirlServicesComposer
{
    /**
     * @var $services
     */
    protected $services;
    /**
     * Constructor
     */
    public function __construct(GirlService $service)
    {
        $this->services = $service;
    }
    /**
     * Get Cities
     *
     * @param  View  $view
     */
    public function compose(View $view)
    {
        $services = Cache::rememberForever('services', function () {
            return $this->services::all();
        });
        $view->with('services', $services);
    }
}
