<?php

namespace App\Http\Controllers\Girls;

use App\Classes\ChunkedFields;
use App\Hookers\Models\CityEloquent\City;
use App\Hookers\Models\GirlEloquent\GirlPublic;
use App\Hookers\Models\GirlEloquent\GirlPublicOption;
use App\Hookers\Services\WebService;
use App\Http\Controllers\Controller;

class GirlAsServiceController extends Controller
{
    public function __construct(WebService $webService)
    {
        $this->webService = $webService;
    }

    public function getList($slug, $id)
    {
        $web = $this->webService;
        $serviceModel = new GirlPublicOption();
        $searchfields = ChunkedFields::chunkedSearchFields();
        $serviceInfo = $serviceModel->findOrFail($id);

        $select = [
            '*',
        ];

        $with = [
            'city',
            'girl_photo',
            'servicesOptions',
        ];

        // Get Girls
        $girlModel = new GirlPublic();

        if ($web->hasCity()) {
            $cityId = $web->currentCityId();
        }
        $serviceId = $serviceInfo->id;

        $girls = $girlModel
            ->select($select)
            ->with($with)
            ->whereHas('city', function ($query) use ($web) {
                if (!empty($web->hasCity())) {
                    $query->where('city_id', $web->currentCityId());
                }

                if (!empty($web)) {
                    if (!$web->hasCity()) {
                        $citiesIds = [];
                        $cityModel = new City();
                        $allCities = $cityModel->select('id')->get();
                        foreach ($allCities as $key => $city) {
                            $citiesIds[] = $city->id;
                        }
                        if (count($citiesIds) > 0) {
                            $query->whereIn('city_id', $citiesIds);
                        }
                    }
                }
            })
            ->whereHas('servicesOptions', function ($query) use ($serviceId) {
                $query->where('service_id', $serviceId)
                    ->onlyCheckedOption();
            })->paginate(config('app.girls_per_page'));

        return view('girls.listing.services.service-list', $searchfields)->with([
            'service' => $serviceInfo,
            'girls' => $girls,
        ]);
    }
}
