<?php

namespace App\Http\Controllers\Girls;

use App\Classes\ChunkedFields;
use App\Hookers\Models\GirlEloquent\GirlPhoto;
use App\Http\Controllers\Controller;

class GirlGalleryController extends Controller
{
    public function getPhotos()
    {
        $searchfields = ChunkedFields::chunkedSearchFields();
        $photos = GirlPhoto::with('girl_public')->orderBy('id', 'desc')->groupBy('girl_public_id')
            ->paginate(config('app.girls_per_page'));

        return view('girls.gallery.gallery-list', $searchfields)->with([
            'photos' => $photos,
        ]);
    }
}
