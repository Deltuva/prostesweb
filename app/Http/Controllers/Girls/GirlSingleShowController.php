<?php

namespace App\Http\Controllers\Girls;

use App\Hookers\Models\GirlEloquent\GirlPublic;
use App\Hookers\Models\GirlEloquent\GirlPublicOption;
use App\Http\Controllers\Controller;

class GirlSingleShowController extends Controller
{
    const COMMENTS_LIMIT = 50;
    /**
     * Show the item for the given id.
     *
     * @param  int  $id
     * @return View
     */
    public function show($id)
    {
        $girlModel = new GirlPublic();
        $comments = $girlModel
          ->findOrFail($id)
          ->comments()
          ->with('user')
          ->orderBy('id', 'asc')
          ->paginate(self::COMMENTS_LIMIT);

        return view('girls.partials.girl-show', [
            'girlItem' => $this->getAnketaDetails($id),
            'girlServices' => $this->getGroupedServicesList($id),
            'comments' => $comments
        ]);
    }

    /**
     * Grouped Services
     *
     * @param GirlService $serviceModel
     * @return array
     */
    public function getGroupedServicesList($id)
    {
        $serviceOptionModel = new GirlPublicOption();
        $services = [];
        $servicesCollection = $serviceOptionModel->select('*')->whereGirlPublicId($id)->orderBy('id', 'asc')->get();
        foreach ($servicesCollection as $key => $collectionItem) {
            if (!is_null($collectionItem)) {
                $services[$collectionItem->getCategoryName()][] = [
                    'id' => $collectionItem->id,
                    'category_id' => $collectionItem->category_id,
                    'service_id' => $collectionItem->service_id,
                    'name' => $collectionItem->service->name,
                    'code' => $collectionItem->service->code,
                    'status' => $collectionItem->status,
                ];
            }
        }

        return $services;
    }

    /**
     * Girl Profile
     *
     * @param $id
     * @return void
     */
    public function getAnketaDetails($id)
    {
        $girlInfo = GirlPublic::with('girl_photo')->findOrFail($id);

        return $girlInfo;
    }
}
