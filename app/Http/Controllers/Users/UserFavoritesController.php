<?php

namespace App\Http\Controllers\Users;

use App\Hookers\Models\FavoriteEloquent\Favorite;
use App\Http\Controllers\Controller;

class UserFavoritesController extends Controller
{
    /**
     * Show the Favorites.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Favorite $favoriteModel)
    {
        $favorites = $favoriteModel->paginate(10);

        return view('user.favorites.favorites-list', [
            'favorites' => $favorites,
        ]);
    }
}
