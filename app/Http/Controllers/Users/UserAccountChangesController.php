<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserAccountChangeRequest;

class UserAccountChangesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function changesUpdate(UserAccountChangeRequest $request)
    {
        $user = auth()
            ->user();

        if ($request->has('name')) {
            $user->name = $request->name;
        }
        if ($request->has('email')) {
            $user->email = $request->email;
        }
        if (\Hash::check($request->current_password, $user->password)) {
            $user->password = bcrypt($request->new_password);
        }

        $user->save();

        $isValidated = $request->validated();
        if ($isValidated) {
            //auth()->logout();
            session()->flash('accountUpdatesuccess', __('Duomenys sėkmingai atnaujinti'));
        }

        return redirect()->back();
    }
}
