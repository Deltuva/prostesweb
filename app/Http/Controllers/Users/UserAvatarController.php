<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\UserAvatarStoreRequest;
use Image;

class UserAvatarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function updateAvatar(UserAvatarStoreRequest $request)
    {
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = 'users/'.time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->fit(300)->save(public_path('/storage/' . $filename));

            $user = auth()->user();
            $user->avatar = $filename;
            $user->save();

            session()->flash('success', __('Avataras sėkmingai atnaujintas'));
        }

        return redirect()->back();
    }
}
