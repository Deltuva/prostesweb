<?php

namespace App\Http\Controllers\Users;

use App\Hookers\Models\UserEloquent\User;
use App\Http\Controllers\Controller;

class UserSettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function edit()
    {
        $authId = auth()
            ->user()
            ->id;

        $userModel = new User();
        $user = $userModel->findOrfail($authId);

        if (!$user) {
            abort(404);
        }

        return view('user.settings.edit')->with([
            'user' => $user,
        ]);
    }

    public function update()
    {
        //
    }
}
