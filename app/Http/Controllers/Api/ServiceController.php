<?php
namespace App\Http\Controllers\Api;

use App\Hookers\Models\GirlEloquent\GirlService;
use App\Http\Controllers\Api\VueController;

class ServiceController extends VueController
{
    /**
     * Get Services Collection
     * @param  GirlService $serviceModel
     * @return json|array
     */

    public function getCollection(GirlService $serviceModel)
    {
        try {
            $responseCollection = [];
            $statusCode = $this->setStatusCode(200);
            $servicesCollection = $serviceModel->select('id', 'category_id', 'name', 'code')->orderBy('id', 'asc')->get();
            foreach ($servicesCollection as $key => $collectionItem) {
                if (!is_null($collectionItem)) {
                    $responseCollection[$collectionItem->getCategoryName()][] = [
                        'id' => $collectionItem->id,
                        'name' => $collectionItem->name,
                        'code' => $collectionItem->code,
                        'checked' => false,
                    ];
                }
            }
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()
                ->json(['data' => $responseCollection, 'status' => $this->getStatusCode()]);
        }
    }
}
