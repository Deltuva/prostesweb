<?php
namespace App\Http\Controllers\Api;

use App\Hookers\Models\FavoriteEloquent\Favorite;
use App\Events\UserFavoriteWasCreated;
use App\Events\UserUnFavoriteWasCreated;
use App\Http\Controllers\Api\VueController;
use Helpers;

class FavoriteController extends VueController
{
    /**
     * Count user favorites
     *
     * @return mixed
     */
    public function countAllUserFavorites()
    {
        try {
            $ip = Helpers::getIp();
            $statusCode = $this->setStatusCode(200);
            $countFavorites = Favorite::whereIp($ip)->count();
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return response()->json(['favorites_counted' => $countFavorites, 'status' => $this->getStatusCode()]);
        }
    }

    /**
     * Post new favorite
     * @param  $favId
     * @return json|status
     */
    public function favoriteNewGirl($favId)
    {
        try {
            $statusCode = $this->setStatusCode(200);
            event(new UserFavoriteWasCreated($favId));
        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return back();
        }
    }
    /**
     * Post new favorite
     * @param  $favId
     * @return json|status
     */
    public function unfavoriteNewGirl($favId)
    {
        try {
            $statusCode = $this->setStatusCode(200);
            event(new UserUnFavoriteWasCreated($favId));

        } catch (Exception $e) {
            $statusCode = $this->setStatusCode(404);
        } finally {
            return back();
        }
    }
}
