<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class VueController extends Controller
{
    protected $statusCode = 200;

    /**
     * getStatusCode function.
     *
     * @access public
     * @return void
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * setStatusCode function.
     *
     * @access public
     * @param mixed $statusCode
     * @return void
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * respondNotFound function.
     *
     * @access public
     * @param string $message (default: 'Not Found!')
     * @return void
     */
    public function respondNotFound($message = 'Not Found!')
    {
        return $this->setStatusCode(404)->respondWithError($message);
    }

    /**
     * respondInternalError function.
     *
     * @access public
     * @param string $message (default: 'Internal Error!')
     * @return void
     */
    public function respondInternalError($message = 'Internal Error!')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }

    /**
     * respond function.
     *
     * @access public
     * @param mixed $data
     * @param mixed $headers (default: [])
     * @return void
     */
    public function respond($data, $headers = [])
    {
        return response()
            ->json($data, $this->getStatusCode(), $headers);
    }

    public function respondWithError($message)
    {
        return $this->respond([
            'error' => [
                'message' => $message,
                'status_code' => $this->getStatusCode(),
            ],
        ]);
    }
}
