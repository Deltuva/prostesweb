<?php

namespace App\Http\Controllers;

use App\Classes\Constants;
use App\Classes\Filter;
use App\Hookers\Models\CategoryEloquent\Category;
use App\Hookers\Models\CityEloquent\City;
use App\Hookers\Models\GirlEloquent\GirlPublic;
use App\Hookers\Models\GirlEloquent\GirlPublicOption;
use App\Hookers\Models\GirlEloquent\GirlService;
use App\Hookers\Models\SearchEloquent\SearchField;
use App\Hookers\Repositories\HookerRepositoryInterface;
use Auth;
use Session;

class HomeController extends Controller
{
    protected $bitchesHookerRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(HookerRepositoryInterface $hooker)
    {
        $this->bitchesHookerRepository = $hooker;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category = null, Category $categoryModel)
    {
        // Return City Id Value
        $city_id = $this->hasCurrentCity();
        $helperFilter = new Filter();

        $category = $categoryModel->whereSlug($category)->first();
        $category_id = (isset($category)) ? $category->id : 100;
        $girlItems = $this->bitchesHookerRepository->getAllGirls($category_id, $city_id);

        // Services Ids
        // if ($helperFilter->hasServicesFilter()) {
        //     $servicesIds = $helperFilter->getServicesIds();
        //     $girlsWithOptions = $this->bitchesHookerRepository->getGirlsOptionsWhereInIds($servicesIds);
        // }
        $girlsServicesGrouped = GirlService::orderBy('id', 'desc')->get()->groupBy('category_id');
        $searchfields = $this->chunkedSearchFields();

        return view('home', $searchfields)->with([
            'abs' => '123456',
        ]);
    }

    /**
     * Cheking Current City Id By Name
     *
     * @return boolean
     */
    public function hasCurrentCity()
    {
        if (!session()->has('city')) {
            $currentCity = City::whereSlug(Constants::TYPE_CITY_VILNIUS)
                ->firstOrFail();
        } else {
            $currentCity = City::whereSlug(session()->get('city'))
                ->firstOrFail();
        }

        return $currentCity->id;
    }

    /**
     * Get Chunked Fields
     *
     * @return void
     */
    public function chunkedSearchFields()
    {
        $searchInfoFields = SearchField::whereCategory(Constants::TYPE_CATEGORY_INFO)->orderBy('id', 'asc')->get();
        $infoCollections = collect($searchInfoFields);
        $searchFieldsInfoData = $infoCollections->chunk(2);
        $searchFieldsInfoData->toArray();

        $searchPriceFields = SearchField::whereCategory(Constants::TYPE_CATEGORY_PRICE)->orderBy('id', 'asc')->get();
        $priceCollections = collect($searchPriceFields);
        $searchFieldsPriceData = $priceCollections->chunk(2);
        $searchFieldsPriceData->toArray();

        $searchDriveFields = SearchField::whereCategory(Constants::TYPE_CATEGORY_DRIVE)->orderBy('id', 'asc')->get();
        $driveCollections = collect($searchDriveFields);
        $searchFieldsDriveData = $driveCollections->chunk(2);
        $searchFieldsDriveData->toArray();

        return [
            'searchFieldsInfoData' => $searchFieldsInfoData,
            'searchFieldsPriceData' => $searchFieldsPriceData,
            'searchFieldsDriveData' => $searchFieldsDriveData,
        ];
    }

    /**
     * Change City
     *
     * @return void
     */
    public function changeCity($city)
    {
        $chekingCity = City::whereSlug($city)->first();
        if ($chekingCity) {
            Session::put('city', $city);
            Session::put('cityName', $chekingCity->getCityName());
        }

        return redirect()->back();
    }

    /**
     * User Logout
     *
     * @return void
     */
    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
