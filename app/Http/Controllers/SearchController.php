<?php

namespace App\Http\Controllers;

class SearchController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchAction()
    {
        $request = request()->all();
        // $array = [];
        // if (!empty($request)) {
        //     foreach ($request['filters']['services'] as $key => $value) {
        //         $array[] = $value;
        //     }
        // }
        // $services = implode(',', $array);
        //dd(implode(',', $array));
        return redirect()->route('home', ['serviceIds' => '1,2,3', 'qfilter' => 'jonas']);
    }
}
