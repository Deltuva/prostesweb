<?php

namespace App\Http\Controllers\Comments;

use App\Hookers\Models\CommentEloquent\Comment;
use App\Hookers\Models\GirlEloquent\GirlPublic;
use App\Http\Controllers\Controller;
use App\Http\Requests\Comments\CommentStoreRequest;
use Helpers;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'store']);
    }

    public function store(CommentStoreRequest $request)
    {
        $authId = auth()
            ->user()
            ->id;

        $girlModel = new GirlPublic();
        $girlPublic = $girlModel->findOrFail($request->girl_id);
        $currentUserIp = Helpers::getIp();

        if ($girlPublic) {
            $comment = new Comment();
            if (isset($request->girl_id)) {
                $comment->girl_public_id = $request->girl_id;
            }
            if (isset($request->comment)) {
                $comment->comment = $request->comment;
            }
            $comment->user_id = $authId;
            $comment->ip = $currentUserIp;
            $comment->girl_public()->associate($girlPublic);
            $comment->save();

            session()->flash('success', __('Komentaras sėkmingai įrašytas'));
        }

        return redirect()->back();
    }
}
