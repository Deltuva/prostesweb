<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composers([
            'App\Http\ViewComposers\CityComposer' => [
                '*',
            ],
            'App\Http\ViewComposers\GirlServicesComposer' => [
                '*',
            ],
        ]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
