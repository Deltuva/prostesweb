<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Hookers\Repositories\HookerRepository;
use App\Hookers\Repositories\HookerRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HookerRepositoryInterface::class, HookerRepository::class);
    }
}
