<?php

use App\Hookers\Models\CategoryEloquent\Category;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Merginos - su privačiais video',
            'slug' => 'video',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Category::create([
            'name' => 'Merginos - Elitinės',
            'slug' => 'elite',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Category::create([
            'name' => 'Merginos - Naujos!',
            'slug' => 'new',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Category::create([
            'name' => 'Atnaujintos',
            'slug' => 'renew',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        Category::create([
            'name' => 'Top 100',
            'slug' => 'top100',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
