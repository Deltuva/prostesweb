<?php

use App\Hookers\Models\SearchEloquent\SearchField;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SearchFieldsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Age
        SearchField::create([
            'category' => 'info',
            'name' => 'age_from',
            'value' => 'Amžius Nuo',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'info',
            'name' => 'age_to',
            'value' => 'Amžius Iki',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Height
        SearchField::create([
            'category' => 'info',
            'name' => 'height_from',
            'value' => 'Ugis Nuo',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'info',
            'name' => 'height_to',
            'value' => 'Ugis Iki',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Weight
        SearchField::create([
            'category' => 'info',
            'name' => 'weight_from',
            'value' => 'Svoris Nuo',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'info',
            'name' => 'weight_to',
            'value' => 'Svoris Iki',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Breast
        SearchField::create([
            'category' => 'info',
            'name' => 'breast_from',
            'value' => 'Krutinė Nuo',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'info',
            'name' => 'breast_to',
            'value' => 'Krutinė Iki',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Price 1hour
        SearchField::create([
            'category' => 'price',
            'name' => '1h_from',
            'value' => '1 val - Nuo',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'price',
            'name' => '1h_to',
            'value' => '1 val - Iki',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Price 2hour
        SearchField::create([
            'category' => 'price',
            'name' => '2h_from',
            'value' => '2 val - Nuo',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'price',
            'name' => '2h_to',
            'value' => '2 val - Iki',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Price 3hour
        SearchField::create([
            'category' => 'price',
            'name' => '3h_from',
            'value' => '3 val - Nuo',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'price',
            'name' => '3h_to',
            'value' => '3 val - Iki',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Price Night
        SearchField::create([
            'category' => 'price',
            'name' => 'night_from',
            'value' => 'Naktys - Nuo',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'price',
            'name' => 'night_to',
            'value' => 'Naktys - Iki',
            'type' => 'input',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Checked fields
        SearchField::create([
            'category' => 'drive',
            'name' => 'to_apartment',
            'value' => 'Į Būtą',
            'type' => 'checkbox',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'drive',
            'name' => 'to_sauna',
            'value' => 'Į Sauną',
            'type' => 'checkbox',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'drive',
            'name' => 'to_hotel',
            'value' => 'Į Viešbutį',
            'type' => 'checkbox',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'drive',
            'name' => 'to_office',
            'value' => 'Į Biurą',
            'type' => 'checkbox',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        SearchField::create([
            'category' => 'drive',
            'name' => 'have_apartments',
            'value' => 'Pas ją',
            'type' => 'checkbox',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
