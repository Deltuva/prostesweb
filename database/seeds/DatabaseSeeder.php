<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->command->info('Categories table seeded.');

        $this->call(CitiesTableSeeder::class);
        $this->command->info('Cities table seeded.');

        $this->call(UsersTableSeeder::class);
        $this->command->info('Users table seeded.');

        $this->call(GirlsServicesCategoriesTableSeeder::class);
        $this->command->info('Girls Services Categories table seeded.');

        $this->call(GirlsServicesTableSeeder::class);
        $this->command->info('Girls Services table seeded.');

        $this->call(GirlsPublicTableSeeder::class);
        $this->command->info('Girls Public table seeded.');

        $this->call(SearchFieldsTableSeeder::class);
        $this->command->info('Search Fields table seeded.');

        $this->call(CommentTableSeeder::class);
        $this->command->info('Comment table seeded.');
    }
}
