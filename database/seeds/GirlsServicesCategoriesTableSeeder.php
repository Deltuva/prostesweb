<?php

use Illuminate\Database\Seeder;
use App\Hookers\Models\GirlEloquent\GirlServiceCategory;
use Carbon\Carbon;

class GirlsServicesCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
          'Pasirinkimas',
          'Malonumas',
          'Finišas',
          'Striptizas',
          'Lesbiečių Šou',
          'Ekstrimas',
          'Auksinis Lietus',
          'Koprofilija',
          'BDSM',
          'Masažas',
          'Papildomai'

        ];
        foreach ($categories as $category) {
            GirlServiceCategory::create([
                'name' => $category,
                'slug' => str_slug($category, '-'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
