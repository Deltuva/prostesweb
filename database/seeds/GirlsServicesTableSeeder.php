<?php

use App\Hookers\Models\GirlEloquent\GirlService;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GirlsServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Pasirinkimas
        GirlService::create([
            'category_id' => 1,
            'name' => 'Klasikinis',
            'code' => 'U_SX_C',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 1,
            'name' => 'Analinis',
            'code' => 'U_SX_A',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 1,
            'name' => 'Grupinis',
            'code' => 'U_SX_G',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 1,
            'name' => 'Lesbiečių',
            'code' => 'U_SX_L',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Malonumas
        GirlService::create([
            'category_id' => 2,
            'name' => 'Su sargių',
            'code' => 'U_MN_P',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 2,
            'name' => 'Be sargio',
            'code' => 'U_MN_BP',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 2,
            'name' => 'Gilus',
            'code' => 'U_MN_G',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 2,
            'name' => 'Mašinoje',
            'code' => 'U_MN_VM',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 2,
            'name' => 'Kunilingas',
            'code' => 'U_MN_K',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 2,
            'name' => 'Anulingas',
            'code' => 'U_MN_A',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Finišas
        GirlService::create([
            'category_id' => 3,
            'name' => 'Į Burną',
            'code' => 'U_OK_R',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 3,
            'name' => 'Ant Veido',
            'code' => 'U_OK_L',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 3,
            'name' => 'Ant krūtinės',
            'code' => 'U_OK_G',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Striptizas
        GirlService::create([
            'category_id' => 4,
            'name' => 'Profesionalus',
            'code' => 'U_ST_P',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 4,
            'name' => 'Ne profesionalus',
            'code' => 'U_ST_NP',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Lesbiečių Šou
        GirlService::create([
            'category_id' => 5,
            'name' => 'Atviras',
            'code' => 'U_LS_O',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 5,
            'name' => 'Lengvas',
            'code' => 'U_LS_L',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Ekstrimas
        GirlService::create([
            'category_id' => 6,
            'name' => 'Straponas',
            'code' => 'U_EX_S',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 6,
            'name' => 'Žaisliukai',
            'code' => 'U_EX_T',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Auksinis Lietus
        GirlService::create([
            'category_id' => 7,
            'name' => 'Daro',
            'code' => 'U_ZD_V',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 7,
            'name' => 'Nedaro',
            'code' => 'U_ZD_P',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Koprofilija
        GirlService::create([
            'category_id' => 8,
            'name' => 'Daro',
            'code' => 'U_K_V',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 8,
            'name' => 'Nedaro',
            'code' => 'U_K_P',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // BDSM
        GirlService::create([
            'category_id' => 9,
            'name' => 'Botagas',
            'code' => 'U_EX_B',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 9,
            'name' => 'Valdovė',
            'code' => 'U_EX_G',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 9,
            'name' => 'Rolių žaidimai',
            'code' => 'U_EX_RG',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 9,
            'name' => 'Dominavimas',
            'code' => 'U_EX_LD',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 9,
            'name' => 'Plakimas',
            'code' => 'U_EX_P',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 9,
            'name' => 'Vergė',
            'code' => 'U_EX_R',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 9,
            'name' => 'Fetišas',
            'code' => 'U_EX_F',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 9,
            'name' => 'Tramplingas',
            'code' => 'U_EX_TR',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Masažas
        GirlService::create([
            'category_id' => 10,
            'name' => 'Klasikinis',
            'code' => 'U_M_C',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 10,
            'name' => 'Profesionalus',
            'code' => 'U_M_P',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 10,
            'name' => 'Atpalaiduojantis',
            'code' => 'U_M_R',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 10,
            'name' => 'Tajų',
            'code' => 'U_M_TJ',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 10,
            'name' => 'Urologinis',
            'code' => 'U_M_U',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 10,
            'name' => 'Vietinis',
            'code' => 'U_M_T',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 10,
            'name' => 'Erotinis',
            'code' => 'U_M_E',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 10,
            'name' => 'Su sakūrų šaka',
            'code' => 'U_M_S',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        // Papildomai
        GirlService::create([
            'category_id' => 11,
            'name' => 'Viešnamis',
            'code' => 'U_AD_EX',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 11,
            'name' => 'Nuotrauka/Video',
            'code' => 'U_AD_PV',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        GirlService::create([
            'category_id' => 11,
            'name' => 'Paslaugos poroms',
            'code' => 'U_AD_P',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
