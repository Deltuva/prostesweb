<?php

use App\Classes\Constants;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GirlsPublicTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Hookers\Models\GirlEloquent\GirlPublic::class, 100)->create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        foreach (\App\Hookers\Models\GirlEloquent\GirlPublic::all() as $public) {
            foreach (\App\Hookers\Models\GirlEloquent\GirlService::all() as $service) {
                \App\Hookers\Models\GirlEloquent\GirlPublicOption::create([
                    'girl_public_id' => $public->id,
                    'category_id' => $service->category_id,
                    'service_id' => $service->id,
                    'status' => Constants::TYPE_SERVICE_UNCHECKED,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
    }
}
