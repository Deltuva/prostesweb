<?php

use App\Hookers\Models\CommentEloquent\Comment;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{
    const COMMENT_LIMIT = 3;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Comment::class, self::COMMENT_LIMIT)->create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
