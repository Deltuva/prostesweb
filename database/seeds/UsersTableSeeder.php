<?php

use App\Hookers\Models\UserEloquent\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Minde',
            'email' => 'deltuva.mindaugas@gmail.com',
            'is_admin' => true,
            'avatar' => 'users/user-male.png',
            'gender' => 'male',
            'password' => bcrypt('1')
        ]);
    }
}
