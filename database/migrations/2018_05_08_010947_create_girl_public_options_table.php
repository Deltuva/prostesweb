<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGirlPublicOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('girl_public_options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('girl_public_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->index();
            $table->integer('service_id')->unsigned()->index();
            $table->enum('status', ['checked', 'unchecked'])->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('girl_public_options');
    }
}
