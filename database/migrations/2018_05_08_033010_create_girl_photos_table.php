<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGirlPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('girl_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('girl_public_id')->unsigned();
            $table->foreign('girl_public_id')->references('id')->on('girl_public')->onDelete('cascade');
            $table->enum('type', ['photo', 'video']);
            $table->string('name');
            $table->boolean('selected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('girl_photos');
    }
}
