<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGirlPublicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('girl_public', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id')->unsigned()->index();
            $table->integer('city_id')->unsigned()->index();

            $table->string('name');
            $table->string('photo');
            $table->string('phone_number')->nullable();
            $table->string('skype')->nullable();

            $table->enum('gender', ['m', 'f']);

            $table->integer('age')->default(0)->index();
            $table->integer('height_size')->default(0)->index();
            $table->integer('breast_size')->default(0)->index();
            $table->integer('weight_size')->default(0)->index();
            $table->integer('clothing_size')->default(0)->index();

            $table->string('region')->nullable()->index();

            $table->integer('price_1hour')->default(0)->index();
            $table->integer('price_2hour')->default(0)->index();
            $table->integer('price_3hour')->default(0)->index();
            $table->integer('price_night')->default(0)->index();

            $table->boolean('can_anal')->default(false)->index();
            $table->boolean('can_to_apartment')->default(false)->index();
            $table->boolean('can_to_sauna')->default(false)->index();
            $table->boolean('can_to_hotel')->default(false)->index();
            $table->boolean('can_to_office')->default(false)->index();
            $table->boolean('have_apartments')->default(false)->index();
            $table->boolean('published')->default(true)->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('girl_public');
    }
}
