<?php

use Faker\Generator as Faker;

$factory->define(App\Hookers\Models\GirlEloquent\GirlPublic::class, function (Faker $faker) {
    return [
      'category_id' => 1,
      'city_id' => $faker->numberBetween(1, 104),
      'name' => $faker->firstNameFemale,
      'photo' => $faker->imageUrl(100, 225),
      'phone_number' => '370'.$faker->numberBetween(10000000, 90000000),
      'gender' => 'f',
      'age' => $faker->numberBetween(18, 36),
      'height_size' => $faker->numberBetween(140, 178),
      'breast_size' => $faker->numberBetween(1, 4),
      'weight_size' => $faker->numberBetween(45, 85),
      'clothing_size' => $faker->numberBetween(45, 65),
      'region' => '-',
      'price_1hour' => $faker->numberBetween(1000, 25000),
      'price_2hour' => $faker->numberBetween(1000, 25000),
      'price_3hour' => $faker->numberBetween(1000, 25000),
      'price_night' => $faker->numberBetween(1000, 25000),
      'can_anal' => $faker->numberBetween(0, 1),
      'can_to_apartment' => $faker->numberBetween(0, 1),
      'can_to_sauna' => $faker->numberBetween(0, 1),
      'can_to_hotel' => $faker->numberBetween(0, 1),
      'can_to_office' => $faker->numberBetween(0, 1),
      'have_apartments' => $faker->numberBetween(0, 1),
    ];
});
