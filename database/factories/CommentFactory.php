<?php

use Faker\Generator as Faker;

$factory->define(App\Hookers\Models\CommentEloquent\Comment::class, function (Faker $faker) {
  return [
      'girl_public_id' => $faker->numberBetween(1, 3),
      'user_id' => $faker->numberBetween(1, 2),
      'comment' => $faker->sentences(6, true),
      'ip' => $faker->ipv4,
  ];
});