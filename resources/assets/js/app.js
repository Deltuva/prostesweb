/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./libs/fontawesome_v5');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('search-component', require('./components/Search/Fields.vue'));
Vue.component('service-component', require('./components/Girl/List.vue'));
Vue.component('favorite-button', require('./components/Favorite/Button.vue'));
Vue.component('favorite-counter', require('./components/Favorite/FavoriteCounter.vue'));

const app = new Vue({
  el: '#app'
});

$(function () {
  $("span.services-count").hide();
  /* ==========================================================================
      Form clear query strings
  ========================================================================== */
  $("form.disable-empty-fields").on("submit", function () {
    $(this).find(":input:not(button)").filter(function () {
      return !this.value
    }).prop("disabled", !0);
    return !0
  }).find(":input:not(button)").prop("disabled", !1);
});