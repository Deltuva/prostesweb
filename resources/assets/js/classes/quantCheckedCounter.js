/**
 * Class QuantCheckedCounter
 */

class QuantCheckedCounter {
  // jquery quant counter plus
  static plus() {
    let quantPlace = $('span.services-count');
    let quantCount = $('span[class="services-count"]').html();
    let currentQuant = parseInt(quantPlace.html());

    currentQuant += 1;
    quantPlace.text(Math.abs(currentQuant));
    quantPlace.show();
  }

  // jquery quant counter minus
  static minus() {
    let quantPlace = $('span.services-count');
    let quantCount = $('span[class="services-count"]').html();
    let currentQuant = parseInt(quantPlace.html());

    currentQuant -= 1;
    quantPlace.text(Math.abs(currentQuant));
  }
}

export default QuantCheckedCounter;