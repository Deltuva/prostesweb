/**
 * Class QuantFavoritedCounter
 */

class QuantFavoritedCounter {
  // jquery quant counter plus
  static plus() {
    let quantPlace = $('span.love-count');
    let quantCount = $('span[class="love-count"]').html();
    let currentQuant = parseInt(quantPlace.html());

    currentQuant += 1;
    quantPlace.text(Math.abs(currentQuant));
    quantPlace.show();
  }

  // jquery quant counter minus
  static minus() {
    let quantPlace = $('span.love-count');
    let quantCount = $('span[class="love-count"]').html();
    let currentQuant = parseInt(quantPlace.html());

    currentQuant -= 1;
    quantPlace.text(Math.abs(currentQuant));
  }
}

export default QuantFavoritedCounter;