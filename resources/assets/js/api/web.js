/**
 * API
 */

class Api {

  static getGirlServiceData(then) {
    return axios(config.AXIOS_GIRL_SERVICE_POINT)
      .then(response => then(response.data.data))
      .catch()
  }

  // User Favorites 
  static getCountUserFavorites(then) {
    return axios(config.AXIOS_FAVORITE_COUNT_POINT)
      .then(response => then(response.data.favorites_counted))
      .catch()
  }

  static favorite(favId) {
    return axios(config.AXIOS_FAVORITE_POINT + favId)
      .then(response => this.isFavorited = true)
      .catch(response => console.log(response))
  }

  static unfavorite(favId) {
    return axios(config.AXIOS_UNFAVORITE_POINT + favId)
      .then(response => this.isFavorited = false)
      .catch(response => console.log(response))
  }
}

export default Api;