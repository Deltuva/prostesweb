/**
 * Defines constants
 */
export const __DM_API_DOMAIN = 'http://localhost:8000/'

// All
export const AXIOS_GIRL_SERVICE_POINT = '/api/girl-services/';
export const AXIOS_FAVORITE_COUNT_POINT = '/api/favorite-count/';
export const AXIOS_FAVORITE_POINT = '/api/favorite/';
export const AXIOS_UNFAVORITE_POINT = '/api/unfavorite/';