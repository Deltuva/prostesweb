<div class="col-12 col-lg-3 col-md-6 col-sm-6 pl-1 pr-1">
  <div class="card mb-4 box-shadow h-305">
    <div class="card-header">
      <div class="d-flex justify-content-between align-items-center">
        @if ($photo->girl_public->name)
          <strong>
            <i class="fas fa-venus mr-2"></i> {{ $photo->girl_public->name }}
          </strong>
        @endif
        <div class="drives d-flex justify-content-between align-items-center">
          <div class="item">
            <i class="fas fa-taxi ml-2 mr-2" style="color: #cccccc;"></i>
          </div>

          @if ($photo->girl_public->cityName)
            <div class="item" style="color: #f40a78;">
              <i class="fas fa-train ml-2"></i>
              <a href="#">{{ $photo->girl_public->cityName }}</a>
            </div>
          @endif
        </div>
      </div>
    </div>
    <div class="card-body">
      <div class="card-holder-block">
        <div class="row no-padding">
          <div class="col-lg-12 col-md-12 col-sm-12 mb-2">
            <div class="photo">
              @if (!is_null($photo->girl_public->photoImage))
                <a href="{{ route('girlItemShow', $photo->girl_public->getId()) }}">
                  <img class="card-img-top img-fluid d-block mx-auto" src="{{ $photo->girl_public->photoImage }}" alt="Nuotrauka">
                </a>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>