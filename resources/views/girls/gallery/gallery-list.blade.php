@extends('layouts.app') 
@section('content')
@include('includes.partials.header-breadcrumbs',[
        'title' => 'Merginų Galerija',
        'breadcrumbs' => [
            [
                'title' => 'Galerija',
                'route' => route('girlGallery')
            ]
        ]
    ])
<section class="advanced bg-fill py-4">
  <div class="container">
    @include('includes.partials.header-advanced-filter')
  </div>
</section>

<section class="girls py-5 bg-list">
  <div class="container">
    <div class="row">
      @each('girls.gallery.partials.gallery-item', $photos, 'photo', 'girls.gallery.partials.gallery-empty')
        
      <div class="col-lg-12">
        {{ $photos->links() }}
      </div>
    </div>
  </div>
</section>
@endsection