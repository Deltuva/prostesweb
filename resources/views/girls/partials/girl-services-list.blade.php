<div class="row">
    @foreach ($girlServices as $groupName => $serviceItem)
      <div class="col-md-3">
        @if (!is_null($groupName))
          <strong class="d-block group-name text-uppercase py-3 pl-3">{{ $groupName }}</strong>
		    @endif

        <ul class="services-list pl-3 pb-4">
          @foreach ($serviceItem as $serviceOption)
            @if (!is_null($serviceOption))
              @if ($serviceOption['status'] === \App\Classes\Constants::TYPE_SERVICE_CHECKED)
                <li class="{{ $serviceOption['status'] === \App\Classes\Constants::TYPE_SERVICE_CHECKED ? 'tunc' : null}}">
                  <i class="fas fa-check pull-left" style="font-size: 8px; margin: 8px 4px 0 0; color: #d41e28;"></i> <a href="{{ route('girlServices', [
                      'slug' => str_slug($serviceOption['name']),
                      'id' => $serviceOption['service_id']
                    ]) }}">{{ $serviceOption['name'] }}</a>
                </li> 
              @else
                <li class="{{ $serviceOption['status'] === \App\Classes\Constants::TYPE_SERVICE_CHECKED ? 'tunc' : null}}">
                  <i class="fas fa-times pull-left" style="font-size: 9px; margin: 8px 4px 0 0; color: #d41e28;"></i> <a href="{{ route('girlServices', [
                    'slug' => str_slug($serviceOption['name']),
                    'id' => $serviceOption['service_id']
                  ]) }}">{{ $serviceOption['name'] }}</a>
                </li>
              @endif
            @endif
          @endforeach
        </ul>
      </div>
    @endforeach
  </div>