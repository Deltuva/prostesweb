<div class="col-12 col-lg-4 col-md-6 col-sm-6 pl-1 pr-1">
  <div class="card mb-4 box-shadow h-305">
    <div class="card-header">
      <div class="d-flex justify-content-between align-items-center">
        @if ($girlItem->name)
          <strong>
            <i class="fas fa-venus mr-2"></i> {{ $girlItem->name }}
          </strong>
        @endif
        <div class="drives d-flex justify-content-between align-items-center">
          <div class="item">
            <i class="fas fa-taxi ml-2 mr-2" style="color: #cccccc;"></i>
          </div>
          @if ($girlItem->cityName)
            <div class="item" style="color: #f40a78;">
              <i class="fas fa-train ml-2"></i>
              <a href="#">{{ $girlItem->cityName }}</a>
            </div>
          @endif
        </div>
      </div>
    </div>
    <div class="card-body">
      <div class="card-holder-block">
        <div class="row no-padding">
          <div class="col-lg-5 col-md-6 col-sm-12 mb-2">
            <div class="photo">
              @if (!is_null($girlItem->photoImage))
                <a href="{{ route('girlItemShow', $girlItem->getId()) }}">
                  <img class="card-img-top img-fluid d-block mx-auto" src="{{ $girlItem->photoImage }}" alt="Nuotrauka">
                </a>
              @endif
              <favorite-button :favorite="{{ $girlItem->getId() }}" 
                :isfavorited="'{{ $girlItem->isFavorited() }}'">
              </favorite-button>
            </div>
          </div>
          <div class="col-lg-7 col-md-6 col-sm-7">
            <div class="d-none d-md-block information">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <span class="phone d-flex justify-content-around align-items-center">
                    @if ($girlItem->phone_number)
                      <i class="fas fa-phone" style="color: #f40a78;" data-fa-transform="rotate-90"></i> {!! $girlItem->phoneNumberLikeSplit($girlItem->phone_number) !!}
                    @endif
                  </span>
                </div>
                <div class="col-md-12 col-sm-12 pl-0">
                  <div class="d-flex align-items-center info-column">
                    <div class="column pull-left">
                      <div class="info-list">
                        @if ($girlItem->femaleAge)
                          <div class="line">
                            <span class="label">Amžius</span>
                            <span class="info">{{ $girlItem->femaleAge }}</span>
                          </div>
                        @endif
                        @if ($girlItem->femaleHeightSize)
                          <div class="line">
                            <span class="label">Ugis</span>
                            <span class="info">{{ $girlItem->femaleHeightSize }}</span>
                          </div>
                        @endif
                        @if ($girlItem->femaleWeightSize)
                          <div class="line">
                            <span class="label">Svoris</span>
                            <span class="info">{{ $girlItem->femaleWeightSize }}</span>
                          </div>
                        @endif
                        @if ($girlItem->femaleBreastSize)
                          <div class="line">
                            <span class="label">Krūtinė</span>
                            <span class="info">{{ $girlItem->femaleBreastSize }}</span>
                          </div>
                        @endif
                      </div>
                    </div>

                    <div class="column pull-left">
                      <div class="info-list">
                        @if ($girlItem->priceForWonHour)
                          <div class="line">
                            <span class="label">1 val</span>
                            <span class="info">{{ $girlItem->priceForWonHour }}</span>
                          </div>
                        @endif
                        @if ($girlItem->priceForTwoHour)
                          <div class="line">
                            <span class="label">2 val</span>
                            <span class="info">{{ $girlItem->priceForTwoHour }}</span>
                          </div>
                        @endif
                        @if ($girlItem->priceForNight)
                          <div class="line">
                            <span class="label">Naktys</span>
                            <span class="info">{{ $girlItem->priceForNight }}</span>
                          </div>
                        @endif
                        @if ($girlItem->canAnalInServicePacket)
                          <div class="line">
                            <span class="label">Analinis</span>
                            <span class="info">{!! $girlItem->canAnalInServicePacket !!}</span>
                          </div>
                        @endif
                      </div>
                    </div>
                  </div>

                  <div class="d-flex align-items-center info-column">
                    <div class="column pull-left">
                      <div class="services-list">
                        @foreach ($girlItem->servicesOptionsPacketWon as $serviceOption)
                          @if (!is_null($serviceOption))
                            @if ($serviceOption->status === \App\Classes\Constants::TYPE_SERVICE_CHECKED)
                              <div class="line">
                                <i class="fas fa-check pull-left" style="font-size: 8px; margin: 5px 4px 0 0; color: #d41e28;"></i>
                                <span>{{ $serviceOption->service->name }}</span>
                              </div>
                            @else
                              <div class="line">
                                <i class="fas fa-times pull-left" style="font-size: 9px; margin: 5px 4px 0 0; color: #d41e28;"></i>
                                <span>{{ $serviceOption->service->name }}</span>
                              </div>
                            @endif
                          @endif
                        @endforeach
                      </div>
                    </div>

                    <div class="column pull-left">
                      <div class="services-list">
                        @foreach ($girlItem->servicesOptionsPacketTwoo as $serviceOption)
                          @if (!is_null($serviceOption))
                            @if ($serviceOption->status === \App\Classes\Constants::TYPE_SERVICE_CHECKED)
                              <div class="line">
                                <i class="fas fa-check pull-left" style="font-size: 8px; margin: 5px 4px 0 0; color: #d41e28;"></i>
                                <span>{{ $serviceOption->service->name }}</span>
                              </div>
                            @else
                              <div class="line">
                                <i class="fas fa-times pull-left" style="font-size: 9px; margin: 5px 4px 0 0; color: #d41e28;"></i>
                                <span>{{ $serviceOption->service->name }}</span>
                              </div>
                            @endif
                          @endif
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>