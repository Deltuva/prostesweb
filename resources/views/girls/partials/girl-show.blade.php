@extends('layouts.app') 
@section('content')
@include('includes.partials.header-breadcrumbs',[
        'title' => $girlItem->name,
        'breadcrumbs' => [
            [
                'title' => 'Visos merginos',
                'route' => route('home')
            ],
            [
                'title' => 'Apie '.$girlItem->name,
                'route' => route('girlItemShow', $girlItem->getId())
            ]
        ]
    ])
<section class="home">
  @include('includes.modals.select-cities')
</section>
<section class="anketa bg-fill py-4">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 order-lg-4 order-8 pl-1 pr-1">
        <div class="photo">
          @if(!$girlItem->photos->isEmpty())
            @foreach ($girlItem->photos as $photo)
              <a href="#">
                <img class="card-img-top img-fluid img-thumbnail d-block mx-auto mb-2" src="{{ asset('storage/'.$photo->name) }}" alt="Nuotrauka">
              </a>
            @endforeach
          @else
            @if (!is_null($girlItem->photoImage))
              <a href="{{ route('girlItemShow', $girlItem->getId()) }}">
                <img class="card-img-top img-fluid img-thumbnail d-block mx-auto mb-2" src="{{ $girlItem->photoImage }}" alt="Nuotrauka">
              </a>
            @endif
          @endif
        </div>
      </div>
      <div class="col-lg-8 order-lg-8 order-4 pl-1 pr-1">
        <div class="card mb-4 box-shadow h-305">
          <div class="card-header">
            <div class="d-flex justify-content-between align-items-center">
              <strong class="text-uppercase">Informacija</strong>

              <div class="drives d-flex justify-content-between align-items-center">
                <div class="item">
                  <i class="fas fa-taxi ml-2 mr-2" style="color: #cccccc;"></i>
                </div>
                @if ($girlItem->cityName)
                  <div class="item" style="color: #f40a78;">
                    <i class="fas fa-train ml-2"></i>
                    <a href="#">{{ $girlItem->cityName }}</a>
                  </div>
                @endif
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="row" style="border-bottom: 1px solid #dcdcdc;">
              <div class="col-lg-8">
                <div class="phone-container">
                  <span class="phone d-flex justify-content-between align-items-center ml-1">
                    @if ($girlItem->phone_number)
                    <i class="fas fa-phone mr-2 align-middle" style="color: #f40a78;" data-fa-transform="rotate-90"></i> {!! $girlItem->phoneNumberLikeSplit($girlItem->phone_number) !!} @endif
                  </span>
                </div>
              </div>
              <div class="col-lg-4">
                <favorite-button :favorite="{{ $girlItem->getId() }}" :isfavorited="'{{ $girlItem->isFavorited() }}'">
                </favorite-button>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4">
                <div class="description-container">
                  <div class="d-flex align-items-center info-column">
                    <div class="column pull-left">
                      <div class="info-list">
                        @if ($girlItem->cityName)
                          <div class="line">
                            <span class="label">
                              <i class="fas fa-angle-right" style="color: #f40a78;"></i> Miestas
                            </span>
                            <span class="info">{{ $girlItem->cityName }}</span>
                          </div>
                        @endif 

                        @if ($girlItem->femaleAge)
                          <div class="line">
                            <span class="label">
                              <i class="fas fa-angle-right" style="color: #f40a78;"></i> Amžius
                            </span>
                            <span class="info">{{ $girlItem->femaleAge }}</span>
                          </div>
                        @endif 

                        @if ($girlItem->femaleHeightSize)
                          <div class="line">
                            <span class="label">
                              <i class="fas fa-angle-right" style="color: #f40a78;"></i> Ugis
                            </span>
                            <span class="info">{{ $girlItem->femaleHeightSize }}</span>
                          </div>
                        @endif 

                        @if ($girlItem->femaleBreastSize)
                          <div class="line">
                            <span class="label">
                              <i class="fas fa-angle-right" style="color: #f40a78;"></i> Krūtinė
                            </span>
                            <span class="info">{{ $girlItem->femaleBreastSize }}</span>
                          </div>
                        @endif 

                        @if ($girlItem->femaleWeightSize)
                          <div class="line">
                            <span class="label">
                              <i class="fas fa-angle-right" style="color: #f40a78;"></i> Svoris
                            </span>
                            <span class="info">{{ $girlItem->femaleWeightSize }}</span>
                          </div>
                        @endif 

                        @if ($girlItem->femaleClothingSize)
                          <div class="line">
                            <span class="label">
                              <i class="fas fa-angle-right" style="color: #f40a78;"></i> Drabužių dydis
                            </span>
                            <span class="info">{{ $girlItem->femaleClothingSize }}</span>
                          </div>
                        @endif
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-8">
                <div class="prices-container py-2">
                  <div class="row no-gutters">
                    <div class="col-6 col-md-3 mb-2 pr-1">
                      <div class="price-block py-2">
                        <i class="fas fa-american-sign-language-interpreting fa-2x mb-2" style="color: {{ $girlItem->priceForWonHour ? '#f40a78' : '#cccccc' }};"></i>
                        <span class="quant-price-hour">1 val</span>
                        <span class="quant-price">&euro; {{ $girlItem->priceForWonHour }}</span>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2 pr-1">
                      <div class="price-block py-2">
                        <i class="fas fa-american-sign-language-interpreting fa-2x mb-2" style="color: {{ $girlItem->priceForTwoHour ? '#f40a78' : '#cccccc' }};"></i>
                        <span class="quant-price-hour">2 val</span>
                        <span class="quant-price">&euro; {{ $girlItem->priceForTwoHour }}</span>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2 pr-1">
                      <div class="price-block py-2">
                        <i class="fas fa-american-sign-language-interpreting fa-2x mb-2" style="color: {{ $girlItem->priceForThreeHour ? '#f40a78' : '#cccccc' }};"></i>
                        <span class="quant-price-hour">3 val</span>
                        <span class="quant-price">&euro; {{ $girlItem->priceForThreeHour }}</span>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2">
                      <div class="price-block py-2">
                        <i class="fas fa-american-sign-language-interpreting fa-2x mb-2" style="color: {{ $girlItem->priceForNight ? '#f40a78' : '#cccccc' }};"></i>
                        <span class="quant-price-hour">Naktys</span>
                        <span class="quant-price">&euro; {{ $girlItem->priceForNight }}</span>
                      </div>
                    </div>
                  </div>
                  <div class="row no-gutters">
                    <div class="col-6 col-md-3 mb-2 pr-1">
                      <div class="price-block py-2">
                        <i class="fas fa-home fa-2x mb-2" style="color: {{ $girlItem->canToHouse ? '#f40a78' : '#cccccc' }};"></i>
                        <span class="quant-price-hour">Į Namus</span>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2 pr-1">
                      <div class="price-block py-2">
                        <i class="fas fa-h-square fa-2x mb-2" style="color: {{ $girlItem->canToHotel ? '#f40a78' : '#cccccc' }};"></i>
                        <span class="quant-price-hour">Į Viešbūtį</span>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2 pr-1">
                      <div class="price-block py-2">
                        <i class="fas fa-building fa-2x mb-2" style="color: {{ $girlItem->canToOffice ? '#f40a78' : '#cccccc' }};"></i>
                        <span class="quant-price-hour">Į Biurą</span>
                      </div>
                    </div>
                    <div class="col-6 col-md-3 mb-2">
                      <div class="price-block py-2">
                        <i class="fas fa-taxi fa-2x mb-2" style="color: {{ $girlItem->canToApartments ? '#f40a78' : '#cccccc' }};"></i>
                        <span class="quant-price-hour">Atvyksta</span>
                      </div>
                    </div>
                    
                    @if (!$girlItem->canAnal)
                      <div class="text-center">
                        * Jokių analinių sekso paslaugų nesuteikiu.
                      </div>
                    @else
                      <div class="text-center">
                          {!! $girlItem->canAnalInServicePacket !!} Analinio sekso paslaugos suteikiamos!
                      </div>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="services-container">
              <div class="title pl-3">
                Paslaugos
              </div>
              @include('girls.partials.girl-services-list')
            </div>
            <div class="comments-container">
              <div class="title pl-3">
                Komentarai <span class="comments-count pull-right">
                  {{ $girlItem->comments->count() }}
                </span>
              </div>
              @include('comments.comment-list')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection