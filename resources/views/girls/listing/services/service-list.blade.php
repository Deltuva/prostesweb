@extends('layouts.app')
@section('content')
@include('includes.partials.header-breadcrumbs',[
        'title' => '<span style="color:#f40a78;">'.$service->service->category->name.' - '.$service->service->name.'</span>',
        'breadcrumbs' => [
            [
                'title' => 'Teikiamos - paslaugos',
                'route' => route('girlServices', [
                  'slug' => str_slug($service->service->name),
                  'id' => $service->service->id
                ])
            ]
        ]
    ])
<section class="advanced bg-fill py-4">
  <div class="container">
    @include('includes.partials.header-advanced-filter')
  </div>
</section>

<section class="girls py-5 bg-list">
  <div class="container">
    <div class="row">
      @each('girls.listing.partials.list-item', $girls, 'girlItem', 'girls.listing.partials.list-empty')

      <div class="col-lg-12">
        {{ $girls->links() }}
      </div>
    </div>
  </div>
</section>
@endsection