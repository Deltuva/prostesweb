<div class="col-12" id="comments" data-id="{{ $comment->id }}">
	<div class="comment-block mb-3">
		<div class="row">
			<div class="col-12 col-md-2 pl-0">
				<div class="comment-block__nickname text-center">
					<img class="avatar mb-2" width="60" src="{{ $comment->user->getAvatar() }}">
					<strong class="d-block">
						@if ($comment->user->isAdministrator())
							<div class="admin-status">
								<span class="badge badge-primary">{{ __('adminas') }}</span>
							</div>
						@endif {{ $comment->user->getName() }}
					</strong>
				</div>
			</div>

			<div class="col-md-8 col-12">
				<div class="comment-block__body">
					<div class="msg">{{ $comment->comment }}</div>
				</div>
			</div>
			<div class="col-md-2 col-12">
				<div class="comment-block__datetime">
					<div class="time">{{ $comment->created_at }}</div>
				</div>
			</div>
		</div>
	</div>
</div>