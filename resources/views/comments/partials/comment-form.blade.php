<div class="comments-form login-box">
	@if (session()->has('success'))
		<div id="callback">
			<div class="alert alert-success rounded-0 mt-5">
				{{ session()->get('success') }}
			</div>
		</div>
	@endif

	<form id="comment-form" action="{{ route('store-comment') }}" method="post">
		@csrf

		<input type="hidden" name="girl_id" value="{{ $girlItem->getId() }}">

		<div class="comment-form__body py-4">
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group">
						<textarea class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}" rows="5" maxlength="1000" autocomplete="off"
						    name="comment" placeholder="{{ __('Komentaras') }}"></textarea>
						@if ($errors->has('comment'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('comment') }}</strong>
							</span>
						@endif
					</div>
				</div>
			</div>
			
			<div class="form-group row pull-right">
				<div class="col-lg-2 col-md-2">
					<button type="submit" class="btn btn-primary btn-lg">{{ __('Komentuoti') }}</button>
				</div>
			</div>
		</div>
	</form>
</div>

@push('scripts') 

@if (count($errors) > 0 || session()->has('success'))
<script>
	function TriggerAlertClose() {
		window.setTimeout(function () {
			$(".alert-success").fadeTo('fast', 0).slideUp('fast', function () {
				$(this).remove();
			});
		}, 1000);
	}

	TriggerAlertClose();

	$('html, body').animate({
		scrollTop: $(".comments-form").offset().top - 140
	}, 1000)
</script>
@endif 

@endpush