<div class="comments-box pl-3 py-3">
	@guest
	<div class="comments-auth">
		Komentuoti gali tik
		<a href="#" data-toggle="modal" data-target="#regModal">
			<strong>registruoti</strong>
		</a> vartotojai.
	</div>
	@endguest

	@auth
	<div class="comments-list
	@if ($girlItem->comments->count() > 10)
	  scroller
	@endif">
		@each('comments.partials.comment-item', $comments, 'comment', 'comments.partials.comment-empty') {{--
		<div class="col-lg-12">
			{{ $comments->links() }}
		</div> --}}
	</div>

	@include('comments.partials.comment-form')
	@endauth
</div>
