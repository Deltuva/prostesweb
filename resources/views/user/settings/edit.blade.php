@extends('layouts.app') 
@section('content')
<section class="home">
  @include('includes.modals.select-cities')

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="breadcrumbs">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Pagrindinis</a>
            </li>
            <li class="breadcrumb-item active">Patikusios merginos</li>
          </ol>
        </div>
        <h1 class="font-weight-bold">Mano meniu</h1>
      </div>
    </div>
</section>

<section class="anketa py-5 bg-list">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 login-box mb-5">
        <form action="{{ route('update-user-avatar') }}" method="post" enctype="multipart/form-data">
          @csrf {{ method_field('PUT') }}

          <div class="text-center">
            <img class="avatar mb-2" width="300" src="{{ $user->getAvatar() }}" alt="{{ __('Avataras') }}">
          </div>

          <div class="form-group {{ $errors->has('avatar') ? 'has-error' : '' }}">
            <input type="file" name="avatar" class="form-control">

            @if ($errors->has('avatar'))
              <span class="invalid-feedback d-block">
                <strong>{{ $errors->first('avatar') }}</strong>
              </span>
            @endif
          </div>

          <button class="btn btn-primary btn-lg">{{ __('Saugoti') }}</button>
        </form>
      </div>
      <div class="col-lg-8 login-box">
        <div class="user-section">
          <form action="{{ route('update-user-account') }}" method="post">
            @csrf 
            {{ method_field('PUT') }}

            <div class="col-12 col-lg-6 no-padding">
              @if (session()->has('accountUpdatesuccess'))
                <div id="callback">
                  <div class="alert alert-success rounded-0">
                    {{ session()->get('accountUpdatesuccess') }}
                  </div>
                </div>
              @endif

              <div class="form-group">
                <label for="name" class="col-form-label">{{ __('Vartotojo vardas') }}:</label>

                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->getName() }}"> 
                @if ($errors->has('name'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="col-12 col-lg-6 no-padding">
              <div class="form-group">
                <label for="email" class="col-form-label fz-5">{{ __('El.paštas') }}:</label>

                <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->getEmail() }}"> 
                @if ($errors->has('email'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="col-12 col-lg-6 no-padding">
              <div class="form-group">
                <label for="gender" class="col-form-label">{{ __('Lytis') }}:</label>

                <select name="gender" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}">
                  @foreach (['male' => __('Vyriška'), 'female' => __('Moteriška')] as $v1 => $v2)
                    @if ($v1 == $user->getGender())
                      <option value="{{ $v1 }}" selected>{{ $v2 }}</option>
                    @else
                      <option value="{{ $v1 }}">{{ $v2 }}</option>
                    @endif
                  @endforeach
                </select>

                @if ($errors->has('gender'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('gender') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="col-12 col-lg-6 no-padding">
              <div class="form-group">
                <label for="current_password" class="col-form-label fz-5">{{ __('Dabartinis slaptažodis') }}:</label>

                <input type="password" class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}" name="current_password" value> 
                @if ($errors->has('current_password'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('current_password') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="col-12 col-lg-6 no-padding">
              <div class="form-group">
                <label for="new_password" class="col-form-label fz-5">{{ __('Naujas slaptažodis') }}:</label>

                <input type="password" class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}" name="new_password" value> 
                @if ($errors->has('new_password'))
                  <span class="invalid-feedback">
                    <strong>{{ $errors->first('new_password') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <button class="btn btn-primary btn-lg">{{ __('Keisti') }}</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection

@push('scripts')
  @if (count($errors) > 0 || session()->has('accountUpdatesuccess'))
    <script>
      function TriggerAlertClose() {
          window.setTimeout(function () {
              $(".alert-success").fadeTo('fast', 0).slideUp('fast', function () {
                  $(this).remove();
              });
          }, 1000);
      }

      TriggerAlertClose();
    </script>
  @endif
@endpush