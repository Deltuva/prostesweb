@extends('layouts.app') @section('content')
<section class="home">
  @include('includes.modals.select-cities')

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-12">
        <div class="breadcrumbs">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Pagrindinis</a>
            </li>
            <li class="breadcrumb-item active">Patikusios merginos</li>
          </ol>
        </div>
        <h1 class="font-weight-bold">Patikusios Merginos (<favorite-counter></favorite-counter>)</h1>
      </div>
    </div>
</section>

<section class="girls py-5 bg-list">
  <div class="container">
    <div class="row">
      @each('user.favorites.partials.favorite-item', $favorites, 'favoriteItem', 'user.favorites.partials.favorite-empty')

      <div class="col-lg-12">
        {{ $favorites->links() }}
      </div>
    </div>
  </div>
</section>
@endsection