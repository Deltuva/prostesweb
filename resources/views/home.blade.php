@extends('layouts.app') 
@section('content')
@if (session()->has('cityName'))
  @include('includes.partials.header-breadcrumbs',[
    'title' => 'Merginos iš '.session()->get('cityName'),
  ])
@endif
<section class="advanced bg-fill py-4">
  <div class="container">
    @include('includes.partials.header-advanced-filter')
  </div>
</section>

<section class="girls py-5 bg-list">
  <div class="container">
    @include('girls.girl-list')
  </div>
</section>
@endsection