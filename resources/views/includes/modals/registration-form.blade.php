<!-- Modal -->
<div class="modal fade" id="regModal" tabindex="-1" role="dialog" aria-labelledby="regModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="regModalLabel">Registracija</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body login-box">
				<form method="POST" action="{{ route('register') }}">
					@csrf

					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label d-block text-md-right">{{ __('Vartotojas') }}
							<strong style="font-size: 80%; color:#dc3545;">*</strong>
						</label>

						<div class="col-md-6">
							<input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"
							    required autofocus> @if ($errors->has('name'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-pašto adresas') }}
							<strong style="font-size: 80%; color:#dc3545;">*</strong>
						</label>

						<div class="col-md-6">
							<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
							    required> @if ($errors->has('email'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="gender" class="col-md-4 col-form-label text-md-right">{{ __('Lytis') }}
							<strong style="font-size: 80%; color:#dc3545;">*</strong>
						</label>

						<div class="col-md-6">
							<select name="gender" class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}">
								<option value>{{ __('-- Pasirinkite --') }}</option>
								<option value="male">{{ __('Vyriška') }}</option>
								<option value="female">{{ __('Moteriška') }}</option>
							</select>
							@if ($errors->has('gender'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('gender') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="adult" class="col-md-4 col-form-label text-md-right">{{ __('Patvirtinu kad man yra +18 metų') }}</label>

						<div class="col-md-6">
							<div class="form-check form-check-inline{{ $errors->has('adult') ? ' is-invalid' : '' }}">
								<input name="adult" class="form-check-input" type="checkbox" value="1">
							</div>
							@if ($errors->has('adult'))
							<span class="d-block invalid-feedback">
								<strong>{{ $errors->first('adult') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Slaptažodis') }}
							<strong style="font-size: 80%; color:#dc3545;">*</strong>
						</label>

						<div class="col-md-6">
							<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password"
							    required> @if ($errors->has('password'))
							<span class="invalid-feedback">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row">
						<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Pakartokite slaptažodį') }}
							<strong style="font-size: 80%; color:#dc3545;">*</strong>
						</label>

						<div class="col-md-6">
							<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
						</div>
					</div>

					<div class="form-group row{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
						<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Apsaugos kodas') }}
							<strong style="font-size: 80%; color:#dc3545;">*</strong>
						</label>

						<div class="col-md-6 pull-center">
							{!! app('captcha')->display() !!} @if ($errors->has('g-recaptcha-response'))
							<span class="help-block">
								<strong style="font-size: 80%; color:#dc3545;">{{ $errors->first('g-recaptcha-response') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group row mb-0">
						<div class="col-md-6 offset-md-4">
							<button type="submit" class="btn btn-primary btn-lg">
								{{ __('Registruotis') }}
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@push('scripts')
<script src='https://www.google.com/recaptcha/api.js?hl=lt'></script>
<script type="text/javascript">
	$('#regModal').modal({
		'show': {
			{
				count($errors - > name) > 0 ? 'true' : 'false'
			}
		}
	});
</script>
@endpush