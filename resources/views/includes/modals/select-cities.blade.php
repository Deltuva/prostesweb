<!-- Modal -->
<div class="modal fade" id="cityModal" tabindex="-1" role="dialog" aria-labelledby="cityModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="cityModalLabel">{{ __('Pasirinkite miestą') }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				@if(!$cities->isEmpty())
				<ul class="cities scroll">
					@foreach ($cities as $city) @if (!is_null($city->getCityName()))
					<li class="cities-item mb-2">
						<a href="{{ route('change-city', $city->getSlug()) }}">{{ $city->getCityName() }}</a>
					</li>
					@else {{-- Reiksmė tuščia! --}} @endif @endforeach
				</ul>
				@endif
			</div>
		</div>
	</div>
</div>