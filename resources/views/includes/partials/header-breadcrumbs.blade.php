<section class="home">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-12">
          <div class="breadcrumbs">
            <ol class="breadcrumb">
              @if(!empty($breadcrumbs))
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Pagrindinis</a></li>
                  @foreach($breadcrumbs as $breadcrumb)
                    <li class="breadcrumb-item active"><a href="{{ $breadcrumb['route'] }}">{{ $breadcrumb['title'] }}</a></li>
                  @endforeach
              @else
                {{-- <li class="breadcrumb-item"><a href="{{ route('home') }}">Pagrindinis</a></li> --}}
              @endif
            </ol>
          </div>

          @if(!empty($title))
            <h1 class="font-weight-bold">{!! $title !!}</h1>
          @endif
        </div>
      </div>
  </section>