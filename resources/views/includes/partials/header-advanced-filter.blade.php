<div class="card-deck mb-3 text-left">
	<div class="card mb-12 box-shadow">
		<div class="card-header">
			<h4 class="my-0 font-weight-normal">
				<i class="fas fa-search mr-2"></i> Paieška
		</div>
		<div class="card-body">
			<form action="" method="GET" class="disable-empty-fields">
				<div class="row mb-3">
					<div class="col-md-12 col-lg-12">
						<div class="row">
							<search-component :fields-list="{{ json_encode($searchFieldsInfoData) }}" :label="'Duomenys'" :icon-class="'fas fa-female mr-2'"
							    :has-services="true">
							</search-component>

							<search-component :fields-list="{{ json_encode($searchFieldsPriceData) }}" :label="'Kainos'" :icon-class="'fas fa-dollar-sign mr-2'"
							    :has-services="false">
							</search-component>

							<search-component :fields-list="{{ json_encode($searchFieldsDriveData) }}" :label="'Išvažiavimas'" :icon-class="'fas fa-taxi mr-2'"
							    :has-services="false">
							</search-component>
						</div>
					</div>
				</div>
				<div class="form-group form-group-md col-12 col-lg-2 pl-0 pr-0 ml-2 mb-2 pull-right">
					<button class="btn btn-clear btn-md btn-block" type="submit">Valyti</button>
				</div>
				<div class="form-group form-group-md col-12 col-lg-2 pl-0 pr-0 mb-2 pull-right">
					<button class="btn btn-search btn-md btn-block" type="submit">Ieškoti</button>
				</div>
			</form>
		</div>
	</div>
</div>