<div class="login-box">
  <form class="form-inline">
    <div class="input-group input-group-md col-12 col-sm-12 col-lg-4 pl-0 pr-0 mr-2 mb-2">
      <input type="text" class="form-control" placeholder="Vartotojas">
    </div>

    <div class="input-group input-group-md col-12 col-sm-12 col-lg-4 pl-0 pr-0 mr-2 mb-2">
      <input type="password" class="form-control" placeholder="Slaptažodis">
    </div>

    <div class="input-group input-group-md col-12 col-lg-3 pl-0 pr-0 mr-2 mb-2">
      <button class="btn btn-primary btn-md btn-block" type="submit">Prisijungti</button>
    </div>
  </form>
</div>