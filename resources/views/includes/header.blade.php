<header>
	<div class="header-top">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="container">
				<a class="navbar-brand" href="#">
					<img src="{{ asset('img/logo.png') }}" style="width: 150px;" class="img-fluid" alt="logo">
				</a>
				<div class="collapse navbar-collapse show" id="navbarText">
					<ul class="navbar-nav mr-auto">
						@guest
						<li class="nav-item active">
							<a class="nav-link" href="#" data-toggle="modal" data-target="#regModal">Ieškai nuotykių? tai prisijunk, karštos merginos tavęs laukia..
								<br/>
								<strong class="fz26" style="text-transform: uppercase">Užregistruoti anketą (+ n-18)</strong>
							</a>
						</li>
						@else
						<li class="nav-item active">
							{{ auth()->user()->getWelcome() }}
						</li>
						@endguest
					</ul>

					@guest @include('includes.partials.header-login') @endguest
				</div>

				@auth
				<ul class="navbar-nav ml-auto">
					<li class="nav-item dropdown dropdown-menu-right">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownUser" role="button" data-toggle="dropdown" aria-haspopup="true"
						    aria-expanded="false">
							<img class="avatar mr-2" width="60" src="{{ auth()->user()->getAvatar() }}">
							<span class="user">{{ Auth::user()->getName() }}</span>
						</a>
						<div class="dropdown-menu dropdown-absolute dropdown-menu-right" aria-labelledby="navbarDropdownUser">
							<a class="dropdown-item" href="#">Balansas: &euro; {{ Auth::user()->getBalance() }}</a>
							<a class="dropdown-item" href="{{ route('userFavorites') }}">Patikusios Merginos (
								<favorite-counter></favorite-counter>)</a>
							<a class="dropdown-item" href="{{ route('userMeniu') }}">Mano meniu</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="{{ route('logout') }}">
								<i class="fas fa-sign-out-alt"></i> Atsijungti</a>
						</div>
					</li>
				</ul>
				@endauth
			</div>
		</nav>
	</div>
	<div class="header-nav">
		<nav class="navbar navbar-expand-lg navbar-custom">
			<div class="container">
				<div class="navbar-country">
					<i class="fas fa-globe"></i>
					<div class="holder pull-right">
						<span class="title">Miestas:</span>
						<a href="#" class="country_switcher" data-toggle="modal" data-target="#cityModal">
							<span>{{ session()->get('cityName') ? session()->get('cityName') : 'Visi' }}</span>
						</a>
					</div>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarProstes">
					<i class="fa fa-bars fa-lg py-1 text-white"></i>
				</button>
				<div class="navbar-collapse collapse" id="navbarProstes">
					<ul class="navbar-nav">
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Kategorijos
							</a>
							<div class="dropdown-menu mt-2" aria-labelledby="navbarDropdown">
								<a class="dropdown-item" href="#">Visos</a>
								<a class="dropdown-item" href="#">Merginos - su privačiais video</a>
								<a class="dropdown-item" href="#">Merginos - Elitinės</a>
								<a class="dropdown-item" href="#">Merginos - Naujos!</a>
								<a class="dropdown-item" href="#">Atnaujintos</a>
								<a class="dropdown-item" href="#">Top 100</a>
							</div>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Masažistės</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">BDSM</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Anketos</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ route('girlGallery') }}">Galerija</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</header>