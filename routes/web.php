<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/pagrindinis/{category?}', 'HomeController@index')->name('home');
Route::get('/patikusios-merginos', 'Users\UserFavoritesController@index')->name('userFavorites');
Route::get('/paslaugos/{slug?}/{id}', 'Girls\GirlAsServiceController@getList')->name('girlServices');
Route::get('/galerija', 'Girls\GirlGalleryController@getPhotos')->name('girlGallery');
Route::get('/mergina/{id}', 'Girls\GirlSingleShowController@show')->name('girlItemShow');
Route::any('/filter', 'SearchController@searchAction')->name('search');
Route::get('/miestas/{city}', 'HomeController@changeCity')->name('change-city');
Route::get('/mano-meniu', 'Users\UserSettingsController@edit')->name('userMeniu');
Route::get('logout', 'HomeController@logout')->name('logout');

Route::put('update-user-account', 'Users\UserAccountChangesController@changesUpdate')->name('update-user-account');
Route::put('update-user-avatar', 'Users\UserAvatarController@updateAvatar')->name('update-user-avatar');
Route::post('store-comment', 'Comments\CommentController@store')->name('store-comment');

// Web Api
Route::group(['prefix' => 'api'], function() {
  Route::get('/girl-services', ['uses' => 'Api\ServiceController@getCollection'])->name('api.girlServices');
  Route::get('/favorite-count', 'Api\FavoriteController@countAllUserFavorites')->name('api.userCountFavorite');
  Route::get('/favorite/{favId}', 'Api\FavoriteController@favoriteNewGirl')->name('api.userFavorite');
  Route::get('/unfavorite/{favId}', 'Api\FavoriteController@unfavoriteNewGirl')->name('api.userUnFavorite');
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
